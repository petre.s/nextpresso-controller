
#ifndef HEAT_REGULATOR_H_INCLUDED
#define HEAT_REGULATOR_H_INCLUDED

#include <PID_v1.h>
#include <Preferences.h>
#include <movingAvg.h>  // https://github.com/JChristensen/movingAvg

#include "EspressoBLEService.h"

#define r_div 979.0  // resistor in the thermistor divisor
// by trial and error, reverse engineering (Silvia) there must
// be an offset between temp on sensor and final brew temp
#define temp_offset 8.7
#define heat_cycle 1000  // ducy cycle for heating has heat_cycle ms period

class HeatRegulator {
   private:
    double setpoint_pid, temp_reading_pid, heating_power_pid;

    movingAvg *coffeeTemp;

    EspressoBLEService *bleService;
    unsigned long lastReadTs = 0;

    bool heat_wait_next_cycle = false;
    bool heat_is_100 = false;

    unsigned long heat_stop_time = 0;
    unsigned long heat_last_start = 0;

    Preferences preferences;
    

   public:
   PID *temp_pid;
    HeatRegulator(EspressoBLEService *bleService) {
        this->bleService = bleService;
        
        preferences.begin("my-app", false);
        
        float kp = preferences.getFloat("hkp", 20);

        float ki = preferences.getFloat("hki", 0.2);

        float kd = preferences.getFloat("hkd", 10);

        // set_target_temp_internal(preferences.getFloat("pidd", 90));
        set_target_temp_internal(90.5);

        temp_pid = new PID(&temp_reading_pid, &heating_power_pid, &setpoint_pid,
                           kp, ki, kd, DIRECT);
        set_high_error_pid_k();
        temp_pid->SetSampleTime(heat_cycle);
        temp_pid->SetMode(AUTOMATIC);
        coffeeTemp = new movingAvg(100);
        coffeeTemp->begin();
        heat_last_start = millis();

        Serial.println("heat using k:");
        Serial.println(kp, 4);
        Serial.println(ki, 4);
        Serial.println(kd, 4);
        
        // last k: 25 0.255 40
        
        //// last k (8martie) 20.0000 0.2000 10.0000
        // last k (9martie) 20.0000, 0.01, 10.0000 best ever no overshoot
    }

    void process_heat() {
        // if (Serial.available()) {
        //     float kp = Serial.parseFloat();
        //     float ki = Serial.parseFloat();
        //     float kd = Serial.parseFloat();

        //     preferences.putFloat("hkp", kp);
        //     preferences.putFloat("hki", ki);
        //     preferences.putFloat("hkd", kd);

        //     Serial.println(kp, 4);
        //     Serial.println(ki, 4);
        //     Serial.println(kd, 4);

        //     preferences.end();
        //     ESP.restart();
        // }

        maybe_set_heat_off();

        
        int sensorValue = analogRead(1);

        sensorValue = coffeeTemp->reading(sensorValue);

        temp_reading_pid = adc_to_celsius(sensorValue);

        if ( setpoint_pid - temp_reading_pid > 1 ) {
            set_high_error_pid_k();
        } else {
            set_low_error_pid_k();
        }
        
        if (!temp_pid->Compute()) {
            return;
        }
        coffeeTemp->reset();

        float heat_power_to_send = 0;

        heat_power_to_send = heating_power_pid;

       
        start_heat_cycle(heat_power_to_send / 255.0);
    }

    void set_low_error_pid_k(){
        this->temp_pid->SetTunings(20.0000, 0.5000, 10.0000);
    }
    void enable(){
        temp_pid->SetMode(AUTOMATIC);
    }

    void disable(){
        temp_pid->SetMode(MANUAL);
    }
    void set_high_error_pid_k(){
        this->temp_pid->SetTunings(20.0000, 0.01, 10.0000);
    }

    float get_actual_temp(){
        return temp_reading_pid - temp_offset;
    }

    float get_set_temp(){
        return setpoint_pid - temp_offset;
    }
    
    void set_and_persist_target_temp(float tmp) {
        set_target_temp_internal(tmp);
        
        preferences.putFloat("pidd", tmp);
    }

   private:
    void handle_heat_power(float percent) {
        // resetting the cycle
        if (heat_last_start < millis() - heat_cycle) {
            heat_last_start = millis();
            heat_wait_next_cycle = false;
        }

        if (heat_wait_next_cycle) {
            return;
        }

        if ((percent > 0) &&
            ((millis() - (float)heat_last_start) / heat_cycle <= percent)) {
            digitalWrite(20, HIGH);
        } else {
            digitalWrite(20, LOW);
            heat_wait_next_cycle = true;
        }
    }

    void start_heat_cycle(float percent) {
        heat_is_100 = percent >= 1.0;

        if (percent == 0) {
            digitalWrite(20, LOW);
            return;
        }

        heat_stop_time = heat_cycle * percent + millis();

        digitalWrite(20, HIGH);
    }

    void maybe_set_heat_off() {
        if (heat_is_100) {
            return;  // heat is to 100%, run without pause
        }

        if (millis() >= heat_stop_time) {
            digitalWrite(20, LOW);
        }
    }

    void set_target_temp_internal(double tmp) {
        if (tmp > 96.5) {
            tmp = 96.5;
        }

        if (tmp < 85) {
            tmp = 85;
        }

        unsigned long ntc_target_value = celsius_to_ntc(tmp + temp_offset);

        unsigned int adc_target = r_div / (ntc_target_value + r_div) * 4095.0;

        setpoint_pid = adc_to_celsius(adc_target);
    }

    static double celsius_to_ntc(double x) {
        return 8803.26726 - 143.606287 * x + 0.622787708 * x * x;
        // https://docs.google.com/spreadsheets/d/1MpVNgQZvnrXrLVmzPr2ILga5KXwghYfCNt9bK0QkjLA/edit#gid=0
    }

    // display only - don't use in calculus!
    static double ntc_to_celsius(double x) {
        if (x < 540) {
            return 130;
        };
        // https://byjus.com/inverse-function-calculator/#:~:text=How%20do%20you%20find%20the,as%20a%20function%20of%20y.
        return 115.293 - 3.23801e-10 * sqrt(1.53145e+19 * x - 8.03794e+21);
    }

    static float adc_to_celsius(int adc_value) {
        if (adc_value < 300) {
            return 10;  // can't map values that low
        }
        float ntc_val = 4095 * r_div / adc_value - r_div;
        return ntc_to_celsius(ntc_val);
    }
};

#endif
