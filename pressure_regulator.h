
#ifndef PRESSURE_REGULATOR_H_INCLUDED
#define PRESSURE_REGULATOR_H_INCLUDED

#include <PID_v1.h>
#include <Preferences.h>
#include <movingAvg.h>  // https://github.com/JChristensen/movingAvg

#include "pump_sigma_delta.h"
#include "pressure_sensor.h"

class PressureRegulator {
   private:
    Preferences preferences;

    PumpSigmaDelta* pump;
    volatile unsigned long lastProcessingTS = 0;
    unsigned int cycle;
    movingAvg* pressure;
    movingAvg* pressureSmooth;
    PID* pid;
    double pressure_setpoint_pid = 0, pressure_reading_pid = 0,
           pump_power_pid = 0;
    long smoothpressureTS = 0;
    float pump_scale_level = 1;

   public:
    PressureRegulator(PumpSigmaDelta* pump,
                      unsigned int cycle) {
        preferences.begin("my-app", false);

        this->pump = pump;
        this->cycle = cycle;
        this->pressure = new movingAvg(10000);
        this->pressureSmooth = new movingAvg(20);
        this->pressure->begin();
        this->pressureSmooth->begin();
        smoothpressureTS = millis();
        // this->pid = new PID(&pressure_reading_pid, &pump_power_pid,
        //                     &pressure_setpoint_pid, 0.2, 0.02, 0, DIRECT);

        // this->pid = new PID(&pressure_reading_pid, &pump_power_pid,
        //                     &pressure_setpoint_pid, 0.25, 0.04, 0, DIRECT);

        // this->pid = new PID(&pressure_reading_pid, &pump_power_pid,
        //                     &pressure_setpoint_pid, 0.25, 0.06, 0, DIRECT);
        // this->pid = new PID(&pressure_reading_pid, &pump_power_pid,
        //                     &pressure_setpoint_pid, 0.25, 0.5, 0, DIRECT);

        // this->pid = new PID(&pressure_reading_pid, &pump_power_pid,
        //                     &pressure_setpoint_pid, 0.1, 1, 0.005, DIRECT);
        // using k:
        // 0.1000
        // 0.1000
        // 0.0050

        // 0.15 0.05 0.005 probably last - NO

        // 0.15 0.03 0.01
        //  0.30 0.03 0.01

        // 0.40 0.06 0.001 a)decrese D b increase i

        // last:
        // pressure using k:
        // 0.3500 0.0500 0.0008
        // 0.3500 0.0070 0.0008
        // 0.0800
        // 0.3000
        // 0.0000
        // 0.0800 0.3000 0.0000

        float kp = preferences.getFloat("kp");

        float ki = preferences.getFloat("ki");

        float kd = preferences.getFloat("kd");

        // float kp = 0.15;

        // float ki = 0.05;

        // float kd = 0.005;

        Serial.println("pressure using k:");
        Serial.println(kp, 4);
        Serial.println(ki, 4);
        Serial.println(kd, 4);

        this->pid = new PID(&pressure_reading_pid, &pump_power_pid,
                            &pressure_setpoint_pid, kp, ki, kd, DIRECT);

        this->pid->SetSampleTime(cycle);
        this->pid->SetMode(MANUAL);
    }

    bool isEnabled() { return (this->pid->GetMode() == AUTOMATIC); }

    void handleState() {
        // if (Serial.available()) {
        //     float kp = Serial.parseFloat();
        //     float ki = Serial.parseFloat();
        //     float kd = Serial.parseFloat();
        //     preferences.putFloat("kp", kp);
        //     preferences.putFloat("ki", ki);
        //     preferences.putFloat("kd", kd);

        //     // Serial.println(kp, 4);
        //     // Serial.println(ki, 4);
        //     // Serial.println(kd, 4);

        //     preferences.end();
        //     ESP.restart();
        // }
        float instantPressure = pressureAnalogRead();
        this->pressure->reading(instantPressure);

        if (millis()- smoothpressureTS > 100){
            this->pressureSmooth->reading(instantPressure);
            smoothpressureTS = millis();
        }
        this->pressure_reading_pid = adc_to_pressure(this->pressure->getAvg());
        bool res = this->pid->Compute();

        if (res) {
            float pumpPower = pump_power_pid / 255.0;

            if (pressure_setpoint_pid == 0) {
                pumpPower = 0;
            }
            this->pump->setPowerLevel(pumpPower);

            this->pressure->reset();
        }
    }

    // void setPressureTargetAdc(int adc_level) {
    //     this->pressure_setpoint_pid = adc_level;
    // }

    void setPressureTargetBar(float pressure_bar) {
        this->pressure_setpoint_pid = pressure_bar;
    }

    float getPressureTargetBar() { return this->pressure_setpoint_pid; }

    float getCurrentPressureBar() { return this->pressure_reading_pid; }

    float getCurrentPressureBarSmooth() {
        if (pressureSmooth->getCount() > 0) {
            return adc_to_pressure(this->pressureSmooth->getAvg());
        } else
            return 0;
    }

    void stop() {
        this->pid->SetMode(MANUAL);
        this->pressure_setpoint_pid = 0;
    }

    void setPumpMaxScale(float scale_level){
        this->pump_scale_level =scale_level;
        this->pid->SetOutputLimits(0, 255.0* scale_level);
    }

    void setGains(double kp, double ki, double kd){
        this->pid->SetTunings(kp, ki, kd);
    }

    void setPreInfusionGains(){
        // for pressured preinfussion was         this->pid->SetTunings(70, 1, 0);
        // this->pid->SetTunings(70, 0, 0);
        this->pid->SetTunings(30, 0, 0);
    }

    void setBrewGains(){
        this->pid->SetTunings(30, 17, 0);
    }

    void start() { 
        this->pump_power_pid = this->pump->getPowerLevel()*255;
        this->pid->SetMode(AUTOMATIC); }

    
};
#endif
// 3.4 -> 1000?
// 4.7 -> 1200
// 6.4 -> 1500
// 8.2 -> 1800
