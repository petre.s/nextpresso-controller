
#ifndef PRESSURE_SENSOR_H_INCLUDED
#define PRESSURE_SENSOR_H_INCLUDED

#define pressure_read_pin 15

int pressureAnalogRead() { return analogRead(pressure_read_pin); }

float pressure_to_adc(float p) { return 171.42 * p + 394.29; }

float adc_to_pressure(unsigned int adc) { return (adc - 394.29) / 171.42; }

float pressureBarRead() { return adc_to_pressure(pressureAnalogRead()); }

#endif