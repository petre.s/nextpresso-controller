
#ifndef TEMPERATURE_CONTROL_H_INCLUDED
#define TEMPERATURE_CONTROL_H_INCLUDED

#include "group_heat_regulator.h"
#include "heat_regulator.h"
#include <Preferences.h>

class TemperatureControl {
   private:
    HeatRegulator *boilerRegulator;
    GroupHeatRegulator *groupHeatRegulator;
    Preferences preferences;
    
    void set_temperature(double target_temp) {
        double groupTemp = target_temp - 4.5;
        double boilerTemp = target_temp - 1.5;
        
        boilerRegulator->set_and_persist_target_temp(boilerTemp);
        groupHeatRegulator->setTemp(groupTemp);
    }

    public:
    TemperatureControl(HeatRegulator *boilerRegulator,
                       GroupHeatRegulator *groupHeatRegulator) {
        this->boilerRegulator = boilerRegulator;
        this->groupHeatRegulator = groupHeatRegulator;
        preferences.begin("tmpctrl", false);
    }

   void set_and_persist_temperature(double target_temp) {
        persist_temperature(target_temp);
        set_temperature(target_temp);
    }

    void persist_temperature(double target_temp) {
        preferences.putFloat("maintemp", target_temp);
    }
  
    double get_set_temp() {
        return preferences.getFloat("maintemp", 50.01);
    }
    

    void set_last_persisted() {
        float temp = preferences.getFloat("maintemp", 92);
        set_temperature(temp);
    }
};
#endif
