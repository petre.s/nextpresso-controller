
#ifndef GROUP_HEAT_REGULATOR_H_INCLUDED
#define GROUP_HEAT_REGULATOR_H_INCLUDED

#include "max6675.h"
#include <PID_v1.h>
#include <Preferences.h>
#include <movingAvg.h>

#define temp_offset 8.7
#define heat_cycle 1000  // ducy cycle for heating has heat_cycle ms period


class GroupHeatRegulator{
   private:
   movingAvg *coffeeTemp;
    double setpoint_pid, temp_reading_pid, heating_power_pid;
    unsigned int lastTime = 0;
    
    int thermoDO = 35;
    int thermoCS = 0;
    int thermoCLK = 45;

    MAX6675 *thermocouple;

    bool heat_wait_next_cycle = false;
    bool heat_is_100 = false;

    unsigned long heat_stop_time = 0;
    unsigned long heat_last_start = 0;

    Preferences preferences;
    

   public:
   uint8_t output_pin = 17;
   PID *temp_pid;
    GroupHeatRegulator() {
        // setpoint_pid = 89;

        this->thermocouple = new MAX6675(thermoCLK, thermoCS, thermoDO);
        preferences.begin("my-app", false);
        
        float kp = preferences.getFloat("gkp", 20);

        float ki = preferences.getFloat("gki", 0.01);

        float kd = preferences.getFloat("gkd", 10);

        coffeeTemp = new movingAvg(3);
        coffeeTemp->begin();
        

        temp_pid = new PID(&temp_reading_pid, &heating_power_pid, &setpoint_pid,
                           kp, ki, kd, DIRECT);
        temp_pid->SetSampleTime(heat_cycle);
        temp_pid->SetMode(AUTOMATIC);
        
        heat_last_start = millis();

        Serial.println("group using k:");
        Serial.println(kp, 4);
        Serial.println(ki, 4);
        Serial.println(kd, 4);
        
    }

    void start(){
        temp_pid->SetMode(AUTOMATIC);
    }

    void stop(){
        temp_pid->SetMode(MANUAL);
    }

    float getCurrentTemp(){
        return this->temp_reading_pid;
    }

    void setTemp(float temp){
        this->setpoint_pid = temp;
    }

    void process_heat() {
        // if (Serial.available()) {
        //     float kp = Serial.parseFloat();
        //     float ki = Serial.parseFloat();
        //     float kd = Serial.parseFloat();

        //     preferences.putFloat("gkp", kp);
        //     preferences.putFloat("gki", ki);
        //     preferences.putFloat("gkd", kd);

        //     Serial.println(kp, 4);
        //     Serial.println(ki, 4);
        //     Serial.println(kd, 4);

        //     preferences.end();
        //     ESP.restart();
        // }

        maybe_set_heat_off();

         unsigned long timeChange = (millis() - lastTime);
            if((timeChange > 300) || (this->coffeeTemp->getCount() == 0)){
                this->coffeeTemp->reading(this->thermocouple->readCelsius()*100);
                lastTime = millis();
            }
        

        temp_reading_pid = this->coffeeTemp->getAvg() / 100.0;

        if ( setpoint_pid - temp_reading_pid > 2 ) {
            set_high_error_pid_k();
        } else {
            set_low_error_pid_k();
        }

        if (!temp_pid->Compute()) {
            return;
        }

        float heat_power_to_send = 0;

        heat_power_to_send = heating_power_pid;

       
        start_heat_cycle(heat_power_to_send / 255.0);
    }

    void set_low_error_pid_k(){
        this->temp_pid->SetTunings(20.0000, 0.01, 10.0000);
    }

    void set_high_error_pid_k(){
        this->temp_pid->SetTunings(1000.0, 0, 0);
    }

    // float get_actual_temp(){
    //     return temp_reading_pid - temp_offset;
    // }

    float get_set_temp(){
        return setpoint_pid;
    }
    
    // void set_and_persist_target_temp(float tmp) {
    //     set_target_temp_internal(tmp);
        
    //     preferences.putFloat("pidd", tmp);
    // }

   private:
    void handle_heat_power(float percent) {
        // resetting the cycle
        if (heat_last_start < millis() - heat_cycle) {
            heat_last_start = millis();
            heat_wait_next_cycle = false;
        }

        if (heat_wait_next_cycle) {
            return;
        }

        if ((percent > 0) &&
            ((millis() - (float)heat_last_start) / heat_cycle <= percent)) {
            digitalWrite(output_pin, HIGH);
            digitalWrite(38, HIGH);
        } else {
            digitalWrite(output_pin, LOW);
            digitalWrite(38, LOW);
            heat_wait_next_cycle = true;
        }
    }

    void start_heat_cycle(float percent) {
        heat_is_100 = percent >= 1.0;

        if (percent == 0) {
            digitalWrite(output_pin, LOW);
            digitalWrite(38, LOW);
            return;
        }

        heat_stop_time = heat_cycle * percent + millis();

        digitalWrite(output_pin, HIGH);
        digitalWrite(38, HIGH);
    }

    void maybe_set_heat_off() {
        if (heat_is_100) {
            return;  // heat is to 100%, run without pause
        }

        if (millis() >= heat_stop_time) {
            digitalWrite(output_pin, LOW);
            digitalWrite(38, LOW);
        }
    }


};


#endif