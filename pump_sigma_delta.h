
#ifndef PUMP_SIGMA_DELTA_H_INCLUDED
#define PUMP_SIGMA_DELTA_H_INCLUDED

#include "pressure_sensor.h"
#include "util.h"

// credits:
// https://deathandthepenguinblog.wordpress.com/2017/05/13/blog-as-you-go-sigma-delta-dac/
class PumpSigmaDelta {
   private:
    volatile float powerLevel = 0;
    volatile float integral = 0;
    byte outputPin;
    volatile bool nextState = false;
    volatile bool currentState = false;
    volatile unsigned long turnOffTime = 0;
    volatile bool turnOffScheduled = false;
    volatile bool nextStateNeedsRecompute = false;
    volatile long lastISRRunTs = 0;
    double volumeTotal = 0;

   public:
    volatile long int missfires = 0;
    PumpSigmaDelta(byte output_pin) { this->outputPin = output_pin; }

    float quantize(float f, int levels) {
        return min(levels - 1.f, max(0.f, floor(f * levels))) / (levels - 1);
    }

    bool getNextPumpState() {
        float output = quantize(integral, 2);
        float difference = powerLevel - output;
        integral += difference;

        return output == 1.0;
    }

    void setPowerLevel(float power_level) {
        if (power_level > 1) {
            power_level = 1;
        }

        if (power_level < 0) {
            power_level = 0;
        }
        this->powerLevel = power_level;
    }

    void positiveSineWareStartInterruptCallback() {
        lastISRRunTs = millis();

        if (!this->nextState && currentState) {
            turnOffScheduled = true;
        }

        if (this->nextState) {
            currentState = true;
            digitalWrite(this->outputPin, HIGH);
        }
        nextStateNeedsRecompute = true;
    }

    float getPowerLevel() { return this->powerLevel; }

    double getVolumeTotal() { return volumeTotal; }

    void resetVolumeTotal() { volumeTotal = 0; }

    void handlePumpState() {
        if (this->turnOffScheduled) {
            turnOffTime = lastISRRunTs + 20.0 * 0.75 + 1;

            unsigned long nowTime = millis();

            unsigned long diff =
                min(nowTime - turnOffTime, turnOffTime - nowTime);

            if (diff <= 1) {
                this->turnOffScheduled = false;
                digitalWrite(this->outputPin, LOW);
                currentState = false;
            } else if (nowTime > turnOffTime) {
                missfires++;
            }
        }

        if (nextStateNeedsRecompute) {
            if (currentState) {
                volumeTotal += getFlowAtPressure(pressureBarRead()) / 50.0;
            }
            this->nextState = getNextPumpState();
            nextStateNeedsRecompute = false;
        }
    }
};
#endif
