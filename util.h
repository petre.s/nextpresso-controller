
#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

class LinearChange {
   public:
    double rate_per_ms = 0;
    double startValue, endValue;
    double long duration_ms;
    unsigned long startTime = 0;

    //    public:
    LinearChange(double start, double end, double duration_ms) {
        this->duration_ms = duration_ms;
        this->endValue = end;
        this->startValue = start;

        rate_per_ms = duration_ms != 0.0 ? (endValue - startValue) / duration_ms
                                         : rate_per_ms;
    }

    void start() {
        if (this->startTime == 0) {
            this->startTime = millis();
        }
    }

    bool isStarted() { return startTime != 0; }

    double getValue() {
        if (duration_ms == 0.0) {
            return endValue;
        }

        if (startTime == 0) {
            return startValue;
        }

        double val = (millis() - startTime) * rate_per_ms + startValue;

        if (startValue > endValue && val < endValue) {
            val = endValue;
        } else if (startValue < endValue && val > endValue) {
            val = endValue;
        }
        return val;
    }
};

double getFlowAtPressure(double pressureBar) {
    double result = 10.8 - 0.72 * pressureBar;

    if (result > 10.8) {
        result = 10.8;
    } else if (result < 0) {
        result = 0;
    }

    return result;
}

#endif