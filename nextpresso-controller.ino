// #ifdef CORE_DEBUG_LEVEL
// #undef CORE_DEBUG_LEVEL
// #endif

// #define CORE_DEBUG_LEVEL 5
// #define LOG_LOCAL_LEVEL ESP_LOG_DEBUG
// #define CONFIG_ARDUHAL_LOG_DEFAULT_LEVEL (5)
// #define ARDUHAL_LOG_LEVEL (5)
#include "EspressoBLEService.h"
#include "brew_strategy.h"
#include "esp_log.h"
#include "flow_rate_regulator.h"
#include "group_heat_regulator.h"
#include "heat_regulator.h"
#include "pressure_regulator.h"
#include "pump_sigma_delta.h"
#include "temperature_control.h"
#include "util.h"
#include "yield_rate_regulator.h"

// HardwareSerial scale_hc05(2);
// scale address +ADDR:98D3:21:F7A7A0

// controller setup was configured to connect to:
// AT+BIND=98D3,21,F7A7A0
static const char *TAG = "AW";

TemperatureControl *temperatureControl = NULL;
EspressoBLEService *bleService = NULL;
BrewStrategyService *brewStrategyService = NULL;
FlowRateRegulator *flowRateRegulator = NULL;

HardwareSerial *controller_hc05;

String ssaaf = "";
float prev_max_pump_scale = -1;
LinearChange *pump_max_scale_change = new LinearChange(1, 1, 1);

float lastWeight = 0;
long lastWeightTS = 0;
bool flush_is_on = false;
unsigned long shotStartTS;
unsigned long bleShotParamLastTs;

// asumption - interrupt is on RISING 0 -> 300V
#define cycle 5
#define sine_period_ms 20

volatile unsigned long last_rise_time = 0;
volatile unsigned long count = 0;

PumpSigmaDelta pump(12);

GroupHeatRegulator *groupHeatRegulator;

PressureRegulator *pressureRegulator;

YieldRateRegulator *yieldRateRegulator;

HeatRegulator *heatRegulator;

int sineWaveInterruptPin = 18;

volatile float targetTemp = 0;
volatile bool targetTempSet = false;

BrewStrategy *strategy = NULL;

bool oldBrewIsOn = false;
unsigned long debounceBrewButtonTS = 0;

void changeTargetTemp(float temp) {
    Serial.print(temp);
    Serial.println(" Temp set");
    targetTemp = temp;
    targetTempSet = true;
}

void weightReceivedCallback(float weight) {
    Serial.print("weight received ");
    Serial.println(weight);
}

// TaskHandle_t readFlowFromSerialTask;

void updateStrategy(String newStrategy) {
    brewStrategyService->setStrategyFromJson(newStrategy);
    // Serial.println(strategy->noOfPhases);
    // Serial.println(strategy->phases[0]->pressureLimit);
}

void setup() {
    Serial.begin(115200);

    esp_log_level_set("*", ESP_LOG_VERBOSE);

    groupHeatRegulator = new GroupHeatRegulator();
    pinMode(12, OUTPUT);  // pump
    pinMode(groupHeatRegulator->output_pin, OUTPUT);
    pinMode(20, OUTPUT);                          // heat
    pinMode(7, OUTPUT);                           //
    pinMode(sineWaveInterruptPin, INPUT_PULLUP);  // sine detection

    // digitalWrite(7, LOW);  // remote brew

    analogReadResolution(12);

    pinMode(46, OUTPUT);  // selenoid

    pinMode(19, INPUT_PULLUP);  // brew button
    pinMode(38, OUTPUT);

    controller_hc05 = new HardwareSerial(2);
    controller_hc05->begin(9600, SERIAL_8N1, 41, 42);

    last_rise_time = millis();

    pressureRegulator = new PressureRegulator(&pump, 200);
    yieldRateRegulator = new YieldRateRegulator(&pump);
    flowRateRegulator = new FlowRateRegulator(&pump);
    brewStrategyService = new BrewStrategyService(pressureRegulator, &pump);
    bleService = new EspressoBLEService("esp231", brewStrategyService);

    bleService->setTempCallback(changeTargetTemp);
    bleService->setWeightCallback(weightReceivedCallback);
    bleService->setStrategyCallback(updateStrategy);

    heatRegulator = new HeatRegulator(bleService);
    temperatureControl =
        new TemperatureControl(heatRegulator, groupHeatRegulator);
    temperatureControl->set_last_persisted();

    // xTaskCreatePinnedToCore(
    //     readFlowFromSerial,      /* Function to implement the task */
    //     "readFlowFromSerial",    /* Name of the task */
    //     10000,                   /* Stack size in words */
    //     NULL,                    /* Task input parameter */
    //     3,                       /* Priority of the task */
    //     &readFlowFromSerialTask, /* Task handle. */
    //     0);                      /* Core where the task should run */

    attachInterrupt(sineWaveInterruptPin, sineWaveInterrupt, CHANGE);
    // xTaskCreatePinnedToCore(attachInterruptTask, "Attach Interrupt Task",
    // 10000, NULL, configMAX_PRIORITIES -1, NULL, 0);

    // Serial.setTxBufferSize(1024);
    // Serial.se
    Serial.println("done init");
}

void sineWaveInterrupt() {
    // hack - configuring interrup at fall does not seeem to get consistent
    // behaviour
    if (digitalRead(sineWaveInterruptPin) == HIGH) {
        return;
    }
    pump.positiveSineWareStartInterruptCallback();
}

// boolean brewOn = false;

unsigned long ccc = millis() + 1000;
unsigned long brewStart;
unsigned long bleSendTS = 0;
double prevPressure;

LinearChange *pressureChange = NULL;

unsigned long delatTs;
boolean pumpStarted = false;

void apply_flow_limit(float targetFlow, int transition_time) {
    float prev_max_level =
        pump_max_scale_change != NULL ? pump_max_scale_change->endValue : 1;

    float new_max_level = flowRateRegulator->flowRateToPumpPower(targetFlow);

    if (prev_max_level == new_max_level) {
        return;
    }

    if (pump_max_scale_change) {
        delete pump_max_scale_change;
    }

    pump_max_scale_change =
        new LinearChange(prev_max_level, new_max_level, transition_time);
    pump_max_scale_change->start();
}

void loop() {
    bool debug_brew_pressed = false;

    if (Serial.available()) {
        float kp = Serial.parseFloat();
        if (kp == 5) {
            debug_brew_pressed = true;
            Serial.println("brewing");
        }
    }

    bleService->handle_BLE();

    if (targetTempSet) {
        temperatureControl->set_and_persist_temperature(targetTemp);
        targetTempSet = false;
    }

    if (millis() - bleSendTS > 1000) {
        bleSendTS = millis();

        bleService->maybeSendTemperatureReading(
            groupHeatRegulator->getCurrentTemp());

        bleService->maybeSendTemperatureTarget(
            temperatureControl->get_set_temp());
        // bleService->sendStrategy();
    }

    if (ccc < millis()) {
        ccc = millis() + 200;

        Serial.print("pump:");
        Serial.print(pump.getPowerLevel() * 10);

        Serial.print(",");
        Serial.print("pressure:");

        Serial.print(pressureRegulator->getCurrentPressureBar() * 10);

        // Serial.print(",");
        // Serial.print("pressureR:");

        // Serial.print(pressureRegulator->getCurrentPressureBar() * 10);

        Serial.print(",");
        Serial.print("ptarget:");
        Serial.print(pressureRegulator->getPressureTargetBar() * 10);

        // Serial.print(",");
        // Serial.print("yieldrate:");
        // Serial.print(
        //     max(-10.0,
        //         min((double)yieldRateRegulator->getCurrentReading(), 10.0)) *
        //     10);

        // Serial.print(",");
        // Serial.print("yield:");
        // Serial.print(yieldRateRegulator->getCurrentWeight());

        // Serial.print(",");
        // Serial.print("yrtarget:");
        // Serial.print(yieldRateRegulator->getTarget() * 10);

        Serial.print(",");
        Serial.print("temp:");
        Serial.print(heatRegulator->get_actual_temp());

        Serial.print(",");
        Serial.print("set_temp:");
        Serial.print(heatRegulator->get_set_temp());

        // Serial.print(",");
        // Serial.print("frtarget:");
        // Serial.print(flowRateRegulator->getTarget());

        Serial.print(",");
        Serial.print("flow:");
        Serial.print(flowRateRegulator->getCurrentReading() * 10);

        Serial.print(",");
        Serial.print("gt:");
        Serial.print(groupHeatRegulator->getCurrentTemp());

        Serial.print(",");
        Serial.print("weight:");
        Serial.print(bleService->getCurrentWeight());

        // Serial.print(",");
        // Serial.print("fp:");
        // Serial.print(heatRegulator->temp_pid->fp);

        // Serial.print(",");
        // Serial.print("fi:");
        // Serial.print(heatRegulator->temp_pid->fi);

        Serial.print(",");
        Serial.print("volume:");
        Serial.print(pump.getVolumeTotal());

        Serial.println();
    }

    // if (ccc < millis()) {
    //     long str = millis();
    //     ccc = millis() + 300;
    //     Serial.print("pump:");
    //     Serial.print(90.999);

    //     Serial.print(",");
    //     Serial.print("pressure:");

    //     Serial.print(920.999);

    //     Serial.print(",");
    //     Serial.print("ptarget:");
    //     Serial.print(900.999);

    //     Serial.print(",");
    //     Serial.print("yieldrate:");
    //     Serial.print(90.999);

    //     Serial.print(",");
    //     Serial.print("yield:");
    //     Serial.print(90.999);

    //     Serial.print(",");
    //     Serial.print("yrtarget:");
    //     Serial.print(9002.2);

    //     Serial.print(",");
    //     Serial.print("temp:");
    //     Serial.print(3232);

    //     Serial.print(",");
    //     Serial.print("set_temp:");
    //     Serial.print(8989);

    //     Serial.print(",");
    //     Serial.print("frtarget:");
    //     Serial.print(8989.23);

    //     Serial.print(",");
    //     Serial.print("flow:");
    //     Serial.print(9090);

    //     // Serial.print(",");
    //     Serial.print("gt:");
    //     Serial.print(3232);

    //     Serial.print(",");
    //     Serial.print("fp:");
    //     Serial.print(9090.21);

    //     Serial.print(",");
    //     Serial.print("fi:");
    //     Serial.print(90900.1);

    //     Serial.print(",");
    //     Serial.print("fd:");
    //     Serial.print(millis()-str);

    //     Serial.println();

    //     Serial.println(pump.missfires);

    // }

    if (((digitalRead(19) == LOW) && (millis() - debounceBrewButtonTS > 300)) ||
        debug_brew_pressed) {
        debounceBrewButtonTS = millis();
        if (flush_is_on) {
            flush_is_on = false;
            stopBrew();
        } else if (!brewStrategyService->isDuringBrew()) {
            brewStrategyService->reset();
            brewStrategyService->start();
            prevPressure = 0;
            digitalWrite(46, HIGH);
            // heatRegulator->disable();
            // groupHeatRegulator->stop();

            shotStartTS = millis();
            bleShotParamLastTs = millis();
            bleService->maybeSendShotParams(
                0, pressureRegulator->getCurrentPressureBar(),
                flowRateRegulator->getCurrentReading());
        } else {
            if (millis() - shotStartTS < 1000) {
                flush_is_on = true;
            } else{
                stopBrew();
            }
        }
    }

    brewStrategyService->handleState();

    if (oldBrewIsOn && !brewStrategyService->isDuringBrew()) {
        stopBrew();
    }
    oldBrewIsOn = brewStrategyService->isDuringBrew();

    if (brewStrategyService->isDuringBrew()) {
        if (millis() - bleShotParamLastTs > 250) {
            bleService->maybeSendShotParams(
                millis() - shotStartTS,
                pressureRegulator->getCurrentPressureBar(),
                flowRateRegulator->getCurrentReading());
            bleShotParamLastTs = millis();
        }

        if (brewStrategyService->getCurrentPhase()->kp > -0.1) {
            pressureRegulator->setGains(
                brewStrategyService->getCurrentPhase()->kp,
                brewStrategyService->getCurrentPhase()->ki,
                brewStrategyService->getCurrentPhase()->kd);
        }

        float targetPressure = brewStrategyService->getCurrentTargetPressure();
        float targetFlow = brewStrategyService->getCurrentTargetFlow();

        prevPressure = pressureRegulator->getPressureTargetBar();

        if ((targetPressure >= -0.1) && (targetFlow > -0.1)) {
            pressureRegulator->setPressureTargetBar(targetPressure);

            apply_flow_limit(targetFlow, 0);

            if (prevPressure == 0 && targetPressure > 0) {
                pump.setPowerLevel(0);
                pressureRegulator->start();
                flowRateRegulator->stop();
            }

        } else if (targetPressure >= -0.1) {
            pressureRegulator->setPressureTargetBar(targetPressure);

            apply_flow_limit(6, 10000);

            if (prevPressure == 0 && targetPressure > 0) {
                pump.setPowerLevel(0);
                pressureRegulator->start();
                flowRateRegulator->stop();
            }
        } else if (targetFlow > -0.1) {
            flowRateRegulator->setFlowTarget(targetFlow);
            flowRateRegulator->start();
            pressureRegulator->stop();
        }

        if (brewStrategyService->needsVolumeReset()) {
            pump.resetVolumeTotal();
        }
    }

    if (pump_max_scale_change) {
        pressureRegulator->setPumpMaxScale(pump_max_scale_change->getValue());
    }

    if (flush_is_on) {
        pump.setPowerLevel(1);
    }
    pump.handlePumpState();
    pressureRegulator->handleState();
    yieldRateRegulator->handleState();
    flowRateRegulator->handleState();
    groupHeatRegulator->process_heat();

    heatRegulator->process_heat();
}

void stopBrew() {
    groupHeatRegulator->start();
    brewStrategyService->stop();
    yieldRateRegulator->stop();
    pressureRegulator->stop();
    flowRateRegulator->stop();
    pump.setPowerLevel(0);
    digitalWrite(46, LOW);
    // heatRegulator->enable();
}

void readFlowFromSerial(void *parameter) {
    while (true) {
        if (controller_hc05->available()) {  // todo check if scale is connected
            String s = controller_hc05->readStringUntil('\n');
            ssaaf = s;

            lastWeight = atof(ssaaf.substring(0, 6).c_str());
            lastWeightTS = atol(ssaaf.substring(6).c_str());

            yieldRateRegulator->addScaleReading(lastWeight, lastWeightTS);
        }
        delay(100);
    }
}
