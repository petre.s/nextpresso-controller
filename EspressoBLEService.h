
#ifndef BLE_SERVICE_H_INCLUDED
#define BLE_SERVICE_H_INCLUDED

#include <BLE2902.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>

#include "brew_strategy.h"

BLEServer* pServer = NULL;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

// #define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b" old
// #define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8" old

#define SERVICE_UUID "17754bf9-fe23-4907-a27f-b0424e1c6713"

#define TEMP_MEASUREMENT_CHARACTERISTIC_UUID \
    "beaffbdc-ad3b-4472-90d0-738397730fd1"
#define TEMP_SET_CHARACTERISTIC_UUID "080d9e6b-0b2a-432c-a6ce-d740b66c2e1d"
#define TEMP_TARGET_CHARACTERISTIC_UUID "9a1af612-c621-46d0-9e26-08d7905c4813"

#define STRATEGY_CHARACTERISTIC_UUID "cc384ad7-2b35-43f5-bc4b-d16e2fa0c55d"

#define WEIGHT_CHARACTERISTIC_UUID "7d340ff6-43e5-436d-a494-8dc6d06233cc"

#define SHOT_PARAMS_CHARACTERISTIC_UUID "323d5bdc-4df0-4fbc-beba-81c65d484ddd"

class MyServerCallbacks : public BLEServerCallbacks {
   private:
    bool deviceConnected;

   public:
    void onConnect(BLEServer* pServer) { this->deviceConnected = true; };

    void onDisconnect(BLEServer* pServer) { this->deviceConnected = false; }

    bool isDeviceConnected() { return this->deviceConnected; }
};

class ValueReceivedCallback : public BLECharacteristicCallbacks {
   public:
    void (*valueReceiveidCallBack)(float) = NULL;
    void onWrite(BLECharacteristic* pCharacteristic) {
        if (pCharacteristic->getLength() != 2) {
            return;
        } else {
            Serial.print("received weight:");
            Serial.println(*pCharacteristic->getData());
        }

        uint8_t* value = pCharacteristic->getData();

        unsigned long heightMostSignificantByte = value[1];
        unsigned long heightLeastSignificantByte = value[0];

        unsigned long heightInCentimeters =
            (heightMostSignificantByte << 8) | heightLeastSignificantByte;
        if (NULL != valueReceiveidCallBack) {
            valueReceiveidCallBack(heightInCentimeters / 100.0);
        }
    }
};

float float_from_bt_array_size_2(uint8_t* data) {
    unsigned long heightMostSignificantByte = data[1];
    unsigned long heightLeastSignificantByte = data[0];

    return ((heightMostSignificantByte << 8) | heightLeastSignificantByte) /
           100.;
}

class StrategyCharacteristicCallback : public BLECharacteristicCallbacks {
   public:
    void (*setStrategyCallback)(String) = NULL;
    void onWrite(BLECharacteristic* pCharacteristic) {
        // if (pCharacteristic->getLength() != 2) {
        //     return;
        // }

        // Serial.println(pCharacteristic->getValue().c_str());

        //
        Serial.println("rec strategy");
        String val =  pCharacteristic->getValue().c_str();
        Serial.println(val);
        this->setStrategyCallback(val);
    }
};

class EspressoBLEService {
   private:
    float latestWeight = 0;
    BLECharacteristic* tempMeasurementCharacteristic = NULL;
    BLECharacteristic* tempSetCharacteristic = NULL;
    BLECharacteristic* tempTargetCharacteristic = NULL;
    BLECharacteristic* strategyCharacteristic = NULL;
    BLECharacteristic* shotParamsCharacteristic = NULL;

    BLECharacteristic* weightCharacteristic = NULL;

    bool oldDeviceConnected = false;
    unsigned long startAdvertisingAt = 0;

    uint32_t value = 0;
    MyServerCallbacks* callback;
    ValueReceivedCallback* tempSetCallback;
    ValueReceivedCallback* weightCallback;
    StrategyCharacteristicCallback* strategyCallback;
    BrewStrategyService* strategy;
    unsigned long lastDeviceConnectionTime;
    bool strategySent = false;

   public:
    EspressoBLEService(std::__cxx11::string deviceName,
                       BrewStrategyService* strategy) {
        BLEDevice::init(deviceName);
        this->strategy = strategy;
        // Create the BLE Server
        pServer = BLEDevice::createServer();

        this->callback = new MyServerCallbacks();

        pServer->setCallbacks(this->callback);

        // Create the BLE Service
        BLEService* pService = pServer->createService(BLEUUID(SERVICE_UUID), 30,0);

        // Create a BLE Characteristic
        tempMeasurementCharacteristic = pService->createCharacteristic(
            TEMP_MEASUREMENT_CHARACTERISTIC_UUID,
            BLECharacteristic::PROPERTY_READ |
                BLECharacteristic::PROPERTY_NOTIFY
            // BLECharacteristic::PROPERTY_WRITE  |
            // BLECharacteristic::PROPERTY_NOTIFY |
            // BLECharacteristic::PROPERTY_INDICATE
        );

        tempMeasurementCharacteristic->addDescriptor(new BLE2902());

        weightCharacteristic = pService->createCharacteristic(
            WEIGHT_CHARACTERISTIC_UUID,
            BLECharacteristic::PROPERTY_READ |
                BLECharacteristic::PROPERTY_WRITE |
                BLECharacteristic::PROPERTY_NOTIFY |
                BLECharacteristic::PROPERTY_INDICATE);
        weightCharacteristic->addDescriptor(new BLE2902());
        this->weightCallback = new ValueReceivedCallback();
        weightCharacteristic->setCallbacks(this->weightCallback);

        tempSetCharacteristic = pService->createCharacteristic(
            TEMP_SET_CHARACTERISTIC_UUID,
            // BLECharacteristic::PROPERTY_READ
            BLECharacteristic::PROPERTY_WRITE |
                BLECharacteristic::PROPERTY_NOTIFY 
            // BLECharacteristic::PROPERTY_INDICATE
        );
        BLE2902 *t = new BLE2902();
        t->setNotifications(true);
        tempSetCharacteristic->addDescriptor(t);
        this->tempSetCallback = new ValueReceivedCallback();
        tempSetCharacteristic->setCallbacks(this->tempSetCallback);

        shotParamsCharacteristic = pService->createCharacteristic(
            SHOT_PARAMS_CHARACTERISTIC_UUID,
            BLECharacteristic::PROPERTY_READ |
                BLECharacteristic::PROPERTY_NOTIFY
        );
        shotParamsCharacteristic->addDescriptor(new BLE2902());



        tempTargetCharacteristic = pService->createCharacteristic(
            TEMP_TARGET_CHARACTERISTIC_UUID,
            BLECharacteristic::PROPERTY_READ |
                BLECharacteristic::PROPERTY_NOTIFY
            // BLECharacteristic::PROPERTY_WRITE  |
            // `BLECharacteristic::PROPERTY_NOTIFY
            // BLECharacteristic::PROPERTY_INDICATE
        );

        BLE2902 *tt = new BLE2902();
        tt->setNotifications(true);
        tempTargetCharacteristic->addDescriptor(tt);
        
        strategyCharacteristic = pService->createCharacteristic(
            STRATEGY_CHARACTERISTIC_UUID,
            BLECharacteristic::PROPERTY_READ |
                BLECharacteristic::PROPERTY_WRITE |
                BLECharacteristic::PROPERTY_NOTIFY);
        this->strategyCallback = new StrategyCharacteristicCallback();
        strategyCharacteristic->setCallbacks(this->strategyCallback);
        strategyCharacteristic->addDescriptor(new BLE2902());

        // Start the service
        pService->start();

        // Start advertising
        BLEAdvertising* pAdvertising = BLEDevice::getAdvertising();
        pAdvertising->addServiceUUID(SERVICE_UUID);
        pAdvertising->setScanResponse(false);
        pAdvertising->setMinPreferred(
            0x0);  // set value to 0x00 to not advertise this parameter
        BLEDevice::startAdvertising();

        Serial.println("Waiting a client connection to notify...");
    }

    void maybeSendTemperatureReading(float temperature) {
        if (this->callback->isDeviceConnected()) {
            uint32_t toSend = (uint32_t)(temperature * 100);
            tempMeasurementCharacteristic->setValue((uint8_t*)&toSend, 4);
            tempMeasurementCharacteristic->notify();
        }
    }

    float getCurrentWeight() {
        if (weightCharacteristic->getLength() == 2) {
            latestWeight =
                float_from_bt_array_size_2(weightCharacteristic->getData());
        }
        return latestWeight;
    }

    void maybeSendTemperatureTarget(float temperature) {
        if (this->callback->isDeviceConnected()) {
            uint32_t toSend = (uint32_t)(temperature * 100);
            tempTargetCharacteristic->setValue((uint8_t*)&toSend, 4);
            tempTargetCharacteristic->notify();
        }
    }

    void maybeSendShotParams(int time, float pressure, float flow) {
        if (this->callback->isDeviceConnected()) {
            char buffer [20];
            sprintf (buffer, "%d %d %d", time, (int)(pressure*100), (int)(flow * 100));
            std::__cxx11::string str = buffer;


            Serial.println(str.c_str());
            shotParamsCharacteristic->setValue(str);
            shotParamsCharacteristic->notify();

        }
    }

    void sendStrategy() {
        Serial.println("sendStrategy");

        const char* strategy = this->strategy->getStrategyJson();
        Serial.println("strategy sent");
        strategyCharacteristic->setValue(strategy);
        strategyCharacteristic->notify();
    }

    void setTempCallback(void (*callback)(float)) {
        this->tempSetCallback->valueReceiveidCallBack = callback;
    }

    void setWeightCallback(void (*callback)(float)) {
        this->weightCallback->valueReceiveidCallBack = callback;
    }

    void setStrategyCallback(void (*callback)(String)) {
        this->strategyCallback->setStrategyCallback = callback;
    }

    void handle_BLE() {
        // notify changed value

        if (!this->callback->isDeviceConnected() && oldDeviceConnected) {
            startAdvertisingAt = millis() + 1000;
            oldDeviceConnected = this->callback->isDeviceConnected();
        }

        if ((startAdvertisingAt != 0) && (millis() > startAdvertisingAt)) {
            pServer->startAdvertising();
            Serial.println("start advertising");
            startAdvertisingAt = 0;
        }
        // connecting
        if (this->callback->isDeviceConnected() && !oldDeviceConnected) {
            // do stuff here on connecting
            oldDeviceConnected = this->callback->isDeviceConnected();
            lastDeviceConnectionTime = millis();
            strategySent = false;
        }

        if (!strategySent && millis() - lastDeviceConnectionTime > 2000) {
            strategySent = true;
            sendStrategy();
        }
    }
};
#endif