
#ifndef BREW_STRATEGY_H_INCLUDED
#define BREW_STRATEGY_H_INCLUDED

#include <ArduinoJson.h>
#include <Preferences.h>

#include "pressure_regulator.h"
#include "pressure_sensor.h"
#include "util.h"
#include "pump_sigma_delta.h"

struct BrewPhase {
    float pressureLimit{-1}, pressureLimitEnd{-1}, yeldRateLimit{-1},
        flowRateLimit{-1}, stopSeconds{-1}, stopYeld{-1}, stopPressure{-1}, stopVolume{-1},
         minTime{-1}, kp{-1}, ki{-1}, kd{-1};
        bool resetVolume{false};
};

struct BrewStrategy {
    BrewPhase** phases;
    int noOfPhases;

    ~BrewStrategy() {
        for (int i = 0; i < noOfPhases; i++) {
            if (phases[i]) {
                delete phases[i];
            }
        }

        delete phases;
    }
};

class BrewStrategyService {
   private:
    BrewStrategy* strategy;
    String strategyStr;
    int phaseIndex = -1;
    float currentYield, currentFlow;
    unsigned long currentPhaseStartTime;

    Preferences preferences;
    LinearChange* pressureChange;
    PressureRegulator *pressureRegulator;
    PumpSigmaDelta *pump;
    bool phaseVolumeReseted = false;
    bool strategyNeedsWrite = false;
    unsigned long pressureBelowTargetLastTS=0;

   public:
    BrewStrategyService(PressureRegulator *pressureRegulator, PumpSigmaDelta *pump) {
        this->pressureRegulator = pressureRegulator;
        this->pump = pump;
        pressureChange = NULL;
        strategy = NULL;
        preferences.begin("strategyy", false);

      
        
        String defaultStrategyJson =
            "{\"ph\":["
            "{\"pl\":8.0,\"frl\":3.0,\"yrl\":null,\"sp\":4,\"sy\":null,\"mt\":4,\"kp\":30,\"ki\":0,\"kd\":0},"
            "{\"pl\":8.0,\"ple\":8.0,\"yrl\":null,\"st\":8,\"rv\":true,\"kp\":30,\"ki\":17,\"kd\":0},"
            "{\"pl\":8.0,\"ple\":5.0,\"yrl\":null,\"st\":10,\"sv\":1000.00},"
            "{\"pl\":5.0,\"yrl\":null,\"st\":null,\"sv\":1000.00}]}";
      

        this->strategyStr = preferences.getString("cstrategy", defaultStrategyJson);
        if (this->strategyStr.length() <5){
            this->strategyStr = defaultStrategyJson;
            Serial.println("using defaultStrategyJson");
        }

        
        // strategyNeedsWrite = true;
            // 
        // Serial.println("from flash");
        // Serial.println(strategyStr);
    //   to test temp
        // const char* strategyJson =
        //     "{\"ph\":["
        //     "{\"pl\":0.0,\"yrl\":null,\"st\":1,\"sy\":null},"
        //     "{\"frl\":3.0,\"yrl\":null,\"st\":7,\"sy\":null},"
        //     "{\"frl\":1.0,\"yrl\":null,\"st\":5,\"sy\":null},"
        //     "{\"frl\":2.0,\"yrl\":null,\"st\":20,\"sy\":null}"
        //     "]}";

        // crash test
        // const char* strategyJson =
        //     "{\"ph\":["
        //     "{\"pl\":0.0,\"yrl\":null,\"st\":1,\"sy\":null},"
        //     "{\"frl\":3.0,\"yrl\":null,\"st\":1,\"sy\":null},"
        //     "{\"frl\":1.0,\"yrl\":null,\"st\":1,\"sy\":null},"
        //     "{\"frl\":2.0,\"yrl\":null,\"st\":1,\"sy\":null}"
        //     "]}";

        // Serial.println(strategyJson);
        setStrategyFromJson(this->strategyStr);
    }

    void setStrategy(BrewStrategy* newStrategy) {
        this->strategy = newStrategy;
    }

    const char* getStrategyJson() {
        return preferences.getString("cstrategy").c_str();
    }

    void setStrategyFromJson(String json) {
        this->strategyNeedsWrite = true;
        this->strategyStr = json;
        Serial.println("StaticJsonDocument<1000> doc;");

        StaticJsonDocument<1000> doc;

        Serial.println("start deserialize");
        Serial.println(json);
        DeserializationError error = deserializeJson(doc, json);
        Serial.println("end deserialize");
        // Test if parsing succeeds.
        if (error) {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.f_str());
            return;
        }

        Serial.println("start parsing strategy from json");
        BrewStrategy* strategy = new BrewStrategy();
        strategy->noOfPhases = doc["ph"].size();
        strategy->phases = new BrewPhase*[doc["ph"].size()];

        for (int i = 0; i < doc["ph"].size(); i++) {
            BrewPhase* phase = new BrewPhase();
            strategy->phases[i] = phase;

            phase->pressureLimit = doc["ph"][i]["pl"].isNull()
                                       ? -1
                                       : doc["ph"][i]["pl"].as<float>();

            phase->pressureLimitEnd = doc["ph"][i]["ple"].isNull()
                                          ? -1
                                          : doc["ph"][i]["ple"].as<float>();

            phase->yeldRateLimit = doc["ph"][i]["yrl"].isNull()
                                       ? -1
                                       : doc["ph"][i]["yrl"].as<float>();

            phase->flowRateLimit = doc["ph"][i]["frl"].isNull()
                                       ? -1
                                       : doc["ph"][i]["frl"].as<float>();

            phase->stopSeconds =
                doc["ph"][i]["st"].isNull() ? -1 : doc["ph"][i]["st"].as<int>();

            phase->minTime =
                doc["ph"][i]["mt"].isNull() ? -1 : doc["ph"][i]["mt"].as<int>();
            
            phase->stopYeld =
                doc["ph"][i]["sy"].isNull() ? -1 : doc["ph"][i]["sy"].as<int>();

            phase->stopPressure = doc["ph"][i]["sp"].isNull()
                                      ? -1
                                      : doc["ph"][i]["sp"].as<float>();

            phase->stopVolume = doc["ph"][i]["sv"].isNull()
                                    ? -1
                                    : doc["ph"][i]["sv"].as<float>();

            phase->resetVolume = doc["ph"][i]["rv"].isNull()
                                     ? false
                                     : doc["ph"][i]["rv"].as<bool>();
            
            phase->kp = doc["ph"][i]["kp"].isNull()
                                    ? -1
                                    : doc["ph"][i]["kp"].as<float>();
            
            phase->ki = doc["ph"][i]["ki"].isNull()
                                    ? -1
                                    : doc["ph"][i]["ki"].as<float>();
            
            phase->kd = doc["ph"][i]["kd"].isNull()
                                    ? -1
                                    : doc["ph"][i]["kd"].as<float>();
        }
        if (this->strategy) {
            delete this->strategy;
            this->strategy = NULL;
        }
        this->setStrategy(strategy);
        Serial.println("done parsing strategy from json");
    }

    void reset() {
        this->phaseIndex = -1;
    }

    void stop() { reset(); }

    void start() {
        this->phaseIndex = -1;
        moveToNextPhase();
    }

    BrewPhase* getCurrentPhase() {
        if (phaseIndex != -1 && phaseIndex < strategy->noOfPhases) {
            return strategy->phases[phaseIndex];
        }
        return NULL;
    }

    bool isDuringBrew() {
        return phaseIndex != -1 && phaseIndex < this->strategy->noOfPhases;
    }

    bool needsVolumeReset() {
        if (!isDuringBrew()) {
            return false;
        }

        bool result = (!phaseVolumeReseted) && (getCurrentPhase()->resetVolume);

        bool pressureReached = this->getCurrentTargetPressure() < this->pressureRegulator->getCurrentPressureBar();

        if (result && pressureReached) {
            phaseVolumeReseted = true;
        }
        return result && pressureReached;
    }

    bool pressureGoalReached() {
        if (getCurrentPhase()->stopPressure > 0) {
            if (this->pressureRegulator->getCurrentPressureBar() <
                getCurrentPhase()->stopPressure) {
                pressureBelowTargetLastTS = millis();
            }

            return (millis() - pressureBelowTargetLastTS) > 100;
        } else {
            return false;
        }
    }

    void moveToNextPhase() {
        phaseIndex++;

        phaseVolumeReseted = false;

        if (!isDuringBrew()) {
            return;
        }

        // Serial.print("moving to phase: ");
        // Serial.println(phaseIndex);
        currentPhaseStartTime = millis();
        if (this->pressureChange) {
            delete this->pressureChange;
            this->pressureChange = NULL;
        }
        // Serial.println("after delete");

        // Serial.println(this->getCurrentPhase()->pressureLimit);
        // Serial.println(this->getCurrentPhase()->pressureLimitEnd);

        float pressureStart = this->getCurrentPhase()->pressureLimit;
        float pressureEnd = this->getCurrentPhase()->pressureLimitEnd > -0.1
                                ? this->getCurrentPhase()->pressureLimitEnd
                                : pressureStart;

        if (pressureStart > -0.1) {
            int timeLimit = this->getCurrentPhase()->stopSeconds > 0
                                ? this->getCurrentPhase()->stopSeconds
                                : 100;
            this->pressureChange =
                new LinearChange(pressureStart, pressureEnd, timeLimit * 1000);
            this->pressureChange->start();
        }
    }

   private:
    bool tryMoveToNextPhase() {
        unsigned long timeInPhaseMillis = millis() - currentPhaseStartTime;

        if ((getCurrentPhase()->minTime > 0) &&
            (timeInPhaseMillis  < getCurrentPhase()->minTime * 1000)) {
            return false;
        }

        if ((getCurrentPhase()->stopSeconds > 0) &&
            (timeInPhaseMillis > getCurrentPhase()->stopSeconds * 1000)) {
            moveToNextPhase();
            return true;
        } else if (pressureGoalReached()) {
            moveToNextPhase();
            return true;
        } else if ((getCurrentPhase()->stopVolume > 0) &&
                   (this->pump->getVolumeTotal() >
                    getCurrentPhase()->stopVolume)) {
            moveToNextPhase();
            return true;
        }
        return false;
    }

   public:
    bool shouldUsePreinfusionGains() { return this->phaseIndex <= 0; }

    float getCurrentTargetPressure() {
        if (!this->isDuringBrew() || pressureChange == NULL) {
            return -1;
        }
        return this->pressureChange->getValue();
    }

    float getCurrentTargetFlow() {
        return this->getCurrentPhase()->flowRateLimit;
    }

    void handleState() {
        if (strategyNeedsWrite){
             size_t l = preferences.putString("cstrategy", this->strategyStr);
             strategyNeedsWrite = false;
             Serial.println("putString");
             Serial.println(this->strategyStr);
             Serial.println(l);
        }

        while (isDuringBrew() && tryMoveToNextPhase()) {
        };
    }
};

#endif
