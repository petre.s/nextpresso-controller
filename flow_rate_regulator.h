
#ifndef FLOW_RATE_REGULATOR_H_INCLUDED
#define FLOW_RATE_REGULATOR_H_INCLUDED

#include <movingAvg.h>  // https://github.com/JChristensen/movingAvg

#include "pressure_sensor.h"
#include "pump_sigma_delta.h"
#include "util.h"

class FlowRateRegulator {
   private:
    float flowSetPoint;
    PumpSigmaDelta* pump;
    boolean enabled;

   public:
    FlowRateRegulator(PumpSigmaDelta* pump) {
        this->pump = pump;
        enabled = false;
    }

    static float flowRateToPumpPower(float flowRate) {
        if (flowRate > 10.8) {
            flowRate = 10.8;
        }
        return flowRate / 10.8;
    }

    void handleState() {
        if (!enabled) {
            return;
        }

        this->pump->setPowerLevel(flowRateToPumpPower(this->flowSetPoint));
    }

    void setFlowTarget(float flowTarget) { this->flowSetPoint = flowTarget; }

    float getTarget() { return flowSetPoint; }

    // calculated flow rate adjusted for pressure
    float getCurrentReading() {
        return pump->getPowerLevel() * getFlowAtPressure(pressureBarRead());
    }

    void start() { enabled = true; }

    void stop() { enabled = false; }
};
#endif
