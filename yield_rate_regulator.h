
#ifndef YIELD_RATE_REGULATOR_H_INCLUDED
#define YIELD_RATE_REGULATOR_H_INCLUDED

#include <HardwareSerial.h>
#include <PID_v1.h>
#include <Preferences.h>
#include <movingAvg.h>  // https://github.com/JChristensen/movingAvg

#include "pump_sigma_delta.h"

#define W_N 10

struct WeightReading {
    float weight;
    long ts;
};

class YieldRateRegulator {
   private:
    WeightReading weightReadings[W_N];
    int currentWeightIndex = -1;

    Preferences preferences;

    PumpSigmaDelta* pump;
    volatile unsigned long lastProcessingTS = 0;
    unsigned int cycle;

    PID* pid;
    double flow_setpoint_pid = 0, flow_reading_pid = 0, pump_power_pid = 0;
    void readFlowFromSerialWork(void* parameter) {}

   public:
    YieldRateRegulator(PumpSigmaDelta* pump, unsigned int cycle = 200) {
        preferences.begin("my-app", false);

        this->pump = pump;
        this->cycle = cycle;

        // float kp = preferences.getFloat("fkp", 1);

        // float ki = preferences.getFloat("fki",0);

        // float kd = preferences.getFloat("fkd",0);

        // float kp = 30;
        // float kp = 35;
        // float kp = 40;
        // float kp = 50; //try with even more
        float kp = 60;

        // float ki = 40;
        float ki = 40;

        float kd = 6;

        // 20 15 6 - best tunning so far

        Serial.println("flow using k:");
        Serial.println(kp, 4);
        Serial.println(ki, 4);
        Serial.println(kd, 4);

        this->pid = new PID(&flow_reading_pid, &pump_power_pid,
                            &flow_setpoint_pid, kp, ki, kd, DIRECT);

        this->pid->SetSampleTime(cycle);
        this->pid->SetMode(MANUAL);
    }

    void handleState() {
        // if (Serial.available()) {
        //     float kp = Serial.parseFloat();
        //     float ki = Serial.parseFloat();
        //     float kd = Serial.parseFloat();
        //     preferences.putFloat("fkp", kp);
        //     preferences.putFloat("fki", ki);
        //     preferences.putFloat("fkd", kd);

        //     Serial.println(kp, 4);
        //     Serial.println(ki, 4);
        //     Serial.println(kd, 4);

        //     preferences.end();
        //     ESP.restart();

        // }

        // this->pressure->reading(analogRead(pressure_reading_pin));

        // flow_reading_pid = 0.4;
        // if (this->controller_hc05->available()) { //todo check if scale is
        // connected
        //     this->flow_reading_pid = this->controller_hc05->parseFloat();
        // }

        bool res = this->pid->Compute();

        if (res) {
            float pumpPower = pump_power_pid / 255.0;

            if (flow_setpoint_pid == 0) {
                pumpPower = 0;
            }

            this->pump->setPowerLevel(pumpPower);

            lastProcessingTS = millis();
            // Serial.print(pump_power_pid/255.0);
            // Serial.print(",");
            // Serial.print(flow_setpoint_pid);
            // Serial.print(",");
            // Serial.println(flow_reading_pid);
        }
    }

    bool isEnabled() { return (this->pid->GetMode() == AUTOMATIC); }

    void setFlowTarget(float flowTarget) {
        this->flow_setpoint_pid = flowTarget;
    }

    void addScaleReading(float weight, long ts) {
        currentWeightIndex++;
        if (currentWeightIndex >= W_N) {
            currentWeightIndex = 0;
        }

        weightReadings[currentWeightIndex].weight = weight;
        weightReadings[currentWeightIndex].ts = ts;

        int oldestIndex = currentWeightIndex + 1;
        if (oldestIndex >= W_N) {
            oldestIndex = 0;
        }

        flow_reading_pid = (weightReadings[currentWeightIndex].weight -
                            weightReadings[oldestIndex].weight) /
                           (weightReadings[currentWeightIndex].ts -
                            weightReadings[oldestIndex].ts) *
                           1000;
    }

    float getTarget() { return flow_setpoint_pid; }

    float getCurrentWeight(){
        return weightReadings[currentWeightIndex].weight;
    }

    float getCurrentReading() { return flow_reading_pid; }

    void start() {
        this->pump_power_pid = this->pump->getPowerLevel() * 255.0;
        this->pid->SetMode(AUTOMATIC);
    }

    void stop() { this->pid->SetMode(MANUAL); }
};
#endif
